﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;


namespace DiscordBot
{
    public class DiscordBot
    {
        DiscordClient client;
        CommandService commands;

        public DiscordBot()
        {
            
            client = new DiscordClient(input =>
            {
                input.LogLevel = LogSeverity.Info;
                input.LogHandler = Log;
            });

            client.UsingCommands(input =>
            {
                input.PrefixChar = '!';
                input.AllowMentionPrefix = true;
                 
            });

            commands = client.GetService<CommandService>();

            commands.CreateCommand("Hello").Do(async (e) =>
            {
                await e.Channel.SendMessage("Word!");
            });

            client.ExecuteAndWait(async () =>
            {
                await client.Connect("Mjg3MDE3MDQ1NzM5MTc1OTM3.C5tPGg.DQBQp116x6cRMTFe01ulqm9dnl0", TokenType.Bot);
            });
        }

        private void Log(object sender, LogMessageEventArgs e)
        {
            Console.WriteLine(e.Message);
        }

    }
}
